cd work
#export CMTCONFIG=${lblogin}
#source LbLogin.sh -c ${lblogin}
source SetupProject.sh Brunel v49r1 #--use "AppConfig "${appconfig}
# Prepare files
echo "from Gaudi.Configuration import *" >> Brunel-Files.py
#Uncomment if you include Moore
echo "EventSelector().Input = [\"DATAFILE='PFN:../files/Boole-Extended.digi' TYP='POOL_ROOTTREE' OPT='READ'\"]" >> Brunel-Files.py
# Run
gaudirun.py ../options/Conditions.py Brunel-Files.py ../options/patchUpgrade1.py $APPCONFIGOPTS/Brunel/MC-WithTruth.py \
 ../options/Brunel-Upgrade-Geom-OnlyVelo.py  $APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py \
 ../options/IgnoreCaliboffDB_LHCBv38r7.py $APPCONFIGOPTS/Brunel/xdst.py ../options/hits_file.py  ../options/VPClusterlink.py    

cp *.xdst ../files
