cd work

lblogin='x86_64-slc6-gcc48-opt'
export CMTCONFIG=${lblogin}
source LbLogin.sh -c ${lblogin}
source SetupProject.sh Gauss v47r2 --use "AppConfig '${appconfig}'"
evtid="10000000"

# Check the opts file
optsfile=$DECFILESROOT/options/${evtid}.py
# Run
gaudirun.py $optsfile ../options/Conditions.py ../options/Gauss-Job.py \
    ../options/Beam-Conditions-Lumi2x.py \
    $LBPYTHIA8ROOT/options/Pythia8.py \
    $APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py \
    ../options/Gauss-Upgrade-Geom-OnlyVelo.py \
    $APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py \
    ../options/IgnoreCaliboffDB_LHCBv38r7.py 
   
# Prepare output
mv `ls *.sim` Gauss.sim


