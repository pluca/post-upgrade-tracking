cd work2

#export appconfig='v3r210' 
lblogin='x86_64-slc6-gcc48-opt'
#export CMTCONFIG=${lblogin}
source LbLogin.sh -c ${lblogin}
source SetupProject.sh Gauss v47r2 
rm *.sim

# Run
gaudirun.py ../options/Conditions.py \
    ../options/Gauss-Job.py \
    $LBPYTHIA8ROOT/options/Pythia8.py \
    $APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py \
    ../options/Gauss-Upgrade-Geom-OnlyVelo.py \
    $APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py \
    ../options/IgnoreCaliboffDB_LHCBv38r7.py \
    ../options/PGuns.py \
    ../options/ParticleGun.py 

# Prepare output
#mv `ls *.sim` ../files/Gauss.sim


