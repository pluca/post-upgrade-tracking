cd work2

lblogin='x86_64-slc6-gcc48-opt'
export CMTCONFIG=${lblogin}
source LbLogin.sh -c ${lblogin}
appconfig='v3r210'
fname='Gauss-100ev'

source SetupProject.sh Boole v29r12 --use "AppConfig "${appconfig}
# Prepare files
#
echo "from Gaudi.Configuration import *" >> Boole-Files.py
fname=/afs/cern.ch/work/p/pluca/public/for_UmbertoAndFederico/run_simulation/files/Gauss-100ev-lumi2x
echo "EventSelector().Input = [\"DATAFILE='PFN:${fname}.sim' TYP='POOL_ROOTTREE' OPT='READ'\"]" >> Boole-Files.py
#
# Run
gaudirun.py ../options/Conditions.py Boole-Files.py $APPCONFIGOPTS/Boole/Default.py \
    ../options/Boole-Upgrade-Geom-OnlyVelo.py ../options/xdigi.py \
    ../options/VPClusterlink.py ../options/IgnoreCaliboffDB_LHCBv38r7.py




