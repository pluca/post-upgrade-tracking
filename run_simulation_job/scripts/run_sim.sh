opts='/afs/cern.ch/work/p/pluca/public/for_UmbertoAndFederico/run_simulation_job/options'

lblogin='x86_64-slc6-gcc48-opt'
export CMTCONFIG=${lblogin}
source LbLogin.sh -c ${lblogin}
appconfig='v3r210'

source SetupProject.sh Gauss v47r2 --use "AppConfig '${appconfig}'"
evtid="10000000"
optsfile=$DECFILESROOT/options/${evtid}.py

# Run Gauss
gaudirun.py $optsfile $opts/Conditions.py \
    Gauss-Job.py Beam-Conditions.py \
    $LBPYTHIA8ROOT/options/Pythia8.py \
    $APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py \
    $opts/Gauss-Upgrade-Geom-OnlyVelo.py \
    $APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py \
    $opts/IgnoreCaliboffDB_LHCBv38r7.py 
   
# Prepare output
mv `ls *.sim` Gauss.sim

# Run Boole
source SetupProject.sh Boole v29r12 --use "AppConfig "${appconfig}

echo "from Gaudi.Configuration import *" >> Boole-Files.py
echo "EventSelector().Input = [\"DATAFILE='PFN:./Gauss.sim' TYP='POOL_ROOTTREE' OPT='READ'\"]" >> Boole-Files.py

gaudirun.py $opts/Conditions.py Boole-Files.py $APPCONFIGOPTS/Boole/Default.py \
    $opts/Boole-Upgrade-Geom-OnlyVelo.py $opts/xdigi.py \
    $opts/VPClusterlink.py $opts/IgnoreCaliboffDB_LHCBv38r7.py




