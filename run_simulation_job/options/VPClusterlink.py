from Gaudi.Configuration  import *
#from Configurables import VPClusterLinker
#seq = GaudiSequencer("LinkVPSeq")
#seq.Members += [VPClusterLinker()]

from Configurables import VPClusterLinker
VPClusterLinker().LinkHits = True

def checker():
    from Configurables import VPClusterMonitor
    GaudiSequencer("CheckPatSeq").Members += [VPClusterMonitor()]

appendPostConfigAction(checker)



