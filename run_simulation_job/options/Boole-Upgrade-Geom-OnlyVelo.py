############################################################################
# File for running Boole with all Baseline Upgrade detectors as of May 2015
############################################################################

from Configurables import Boole, CondDB

CondDB().Upgrade = True
Boole().DataType = "Upgrade"

Boole().DetectorDigi = ['VP','UT', 'FT']
Boole().DetectorLink = ['VP','UT', 'FT', 'Tr']
Boole().DetectorMoni = ['VP','UT', 'FT']
