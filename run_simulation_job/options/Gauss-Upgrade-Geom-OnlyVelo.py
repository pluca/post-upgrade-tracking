############################################################################
# File for running Gauss with all Baseline Upgrade detectors as of May 2015
############################################################################

from Configurables import Gauss, CondDB

CondDB().Upgrade = True

Gauss().DetectorGeo  = { "Detectors": ['VP','UT', 'FT'] }
Gauss().DetectorSim  = { "Detectors": ['VP','UT', 'FT'] }
Gauss().DetectorMoni = { "Detectors": ['VP','UT', 'FT'] }

Gauss().DataType = "Upgrade"
