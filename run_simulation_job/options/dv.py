######################################################################
#
# Bd2KstEE MC12 MU
#
######################################################################
Test = False
#StrippingVersion = 'stripping20'
#StrippingVersion = 'stripping20r1p2'
StrippingVersion = 'stripping21'
######################################################################
from Gaudi.Configuration       import *
from GaudiKernel.SystemOfUnits import *
######################################################################
#
# Run Stripping
#
######################################################################
from StrippingArchive.Utils import buildStreams
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive import strippingArchive
from StrippingConf.Configuration import StrippingStream
from Configurables import ProcStatusCheck
from StrippingConf.Configuration import StrippingConf
Streams = buildStreams( stripping = strippingConfiguration( StrippingVersion ),
                        archive   = strippingArchive( StrippingVersion ) )
MyStream = StrippingStream( "MyStream" )
line = "B2XMuMu_Line"
#MyLines  = [ "StrippingBu2LLK_eeLine2" ]
MyLines  = [ "Stripping"+line ]
for Stream in Streams:
    for Line in Stream.lines:
        if Line.name() in MyLines:
            MyStream.appendLines( [ Line ] )
            filterBadEvents = ProcStatusCheck()
            ReStripping = StrippingConf( Streams           = [ MyStream ],
                                         MaxCandidates     = 2000,
                                         AcceptBadEvents   = False,
                                         BadEventSelection = filterBadEvents,
                                         HDRLocation       = "SomeNonExistingLocation" )
#if ( StrippingVersion == 'stripping21' ) :
#    from Configurables import PhysConf
#    PhysConf().CaloReProcessing = True

######################################################################
#
# Define the Sequencer
#
######################################################################
from Configurables import GaudiSequencer
Sequencer = GaudiSequencer( "Sequencer" )
######################################################################
#
# Define the Decay Tree Tuple
#
######################################################################
from Configurables import DecayTreeTuple
Tuple = DecayTreeTuple()
#Tuple.Inputs   = [ "Phys/Bu2LLK_eeLine2/Particles" ]
Tuple.Inputs   = [ "Phys/"+line+"/Particles" ]
Tuple.Decay    = "[ X+ ]CC"
######################################################################
from Configurables import TupleToolDecay
Tuple.ToolList += [
    "TupleToolANNPID",
    "TupleToolBremInfo",
    "TupleToolEventInfo",
    "TupleToolGeometry",
    "TupleToolKinematic",
    "TupleToolPid",
    "TupleToolPrimaries",
    "TupleToolPropertime",
    "TupleToolRecoStats",
    "TupleToolTrackInfo"
    ]
######################################################################
######################################################################
#
# Configure the Tuple Tool MCTruth
#
######################################################################
from Configurables import TupleToolMCTruth
Tuple.addTool( TupleToolMCTruth )
Tuple.ToolList                  += [ "TupleToolMCTruth" ]
Tuple.TupleToolMCTruth.ToolList += [
    "MCTupleToolHierarchy",
    "MCTupleToolKinematic"
    ]
######################################################################
#
# Define the MCDecay Tree Tuple
#
######################################################################
from Configurables import MCDecayTreeTuple
MCTuple          = MCDecayTreeTuple()
MCTuple.Decay    = "[ X+ ]CC"
######################################################################
MCTuple.ToolList += [
    "MCTupleToolKinematic",
    "MCTupleToolPID"
    ]

######################################################################
#
# Define the Event Tuple
#
######################################################################
from Configurables import EventTuple
ETuple = EventTuple()
######################################################################
#
# Configure DaVinci
#
######################################################################

EventSelector().Input = ["DATAFILE='PFN:test/Brunel.xdst' TYP='POOL_ROOTTREE' OPT='READ'"]

from Configurables import DaVinci
DaVinci().appendToMainSequence( [ ReStripping.sequence() ] )
DaVinci().UserAlgorithms = [ Sequencer, Tuple, MCTuple, ETuple ]
DaVinci().TupleFile  = "DVNtuples.root"
DaVinci().EvtMax     = -1
DaVinci().PrintFreq  = 10000
DaVinci().DataType       = "2012"
DaVinci().Simulation     = True
DaVinci().Lumi           = False
#DaVinci().DDDBtag        = "Sim08-20150424"
#DaVinci().CondDBtag      = "Sim08-20140825-vc-mu100"
######################################################################
