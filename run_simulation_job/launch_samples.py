#! /usr/bin/env python

import os
import re
import subprocess as sub
import sys

nevts_perjob = 1
start_job = 20
n_jobs = 1
#nevts_perjob = 1
#n_jobs = 1

local = ""
#local = "--local"        # Runs in local 
#local = "--interactive"  # Runs in a way that bypasses the bacth system. Sometimes needed but not recommended.

pwd = os.environ["PWD"]
opts = pwd+"/options"
scripts = pwd+"/scripts"

#sample = "norm_lumi" # norm_lumi or lumi_x{n} (as lumi_x2, lumi_x1.5, etc)
sample = "lumi_x3.2"
for s in range(start_job,start_job+n_jobs) :

    lumi = 1
    if "norm" not in sample :
        matches = re.findall(r'\d+.\d+',sample)
        if len(matches)==1 :
            lumi = matches[0]
            print "Requested luminosity = x"+lumi+" wrt upgrade" 
        else :
            print "Sample name wrong"
            os.exit()

    sub.call("cp {opts}/Gauss-Job.py .".format(opts=opts),shell=True)
    sub.call("cp {scripts}/run_sim.sh .".format(scripts=scripts),shell=True)
    sub.call("cp {opts}/Beam-Conditions.py .".format(opts=opts),shell=True)
    sub.call("sed -i 's|nEvts = .*|nEvts = %i|' Gauss-Job.py" % nevts_perjob,shell=True)
    sub.call("sed -i 's|GaussGen.FirstEventNumber =.*|GaussGen.FirstEventNumber = %i|' Gauss-Job.py" % (1 + s*nevts_perjob) , shell=True)
    sub.call("sed -i 's|$opts/|"+opts+"/|' run_sim.sh",shell=True)
    
    if lumi != 1 :
        sub.call("sed -i 's|Gauss()\.Luminosity.*|\
Gauss()\.Luminosity = {lumi} * 0.8338*(10**30) \
/(SystemOfUnits\.cm2*SystemOfUnits\.s)|' Beam-Conditions.py".format(lumi=lumi),shell=True)
    
    runcmd =  "python submit.py {local}".format(local=local)                                  # Submit method         
    #runcmd += " -s 'chmod +x run_sim.sh;' "                                                  # Custom setup   
    runcmd += " -d samples/{sample} -n {subj} 'run_sim.sh' ".format(sample=sample,subj=s)     # Folders' names and run command
       
       # Options to be copied over
    runcmd += " Gauss-Job.py "
    runcmd += " Beam-Conditions.py "

        #Run
    sub.call(runcmd,shell=True)
    sub.call("rm Gauss-Job.py; rm run_sim.sh; rm Beam-Conditions.py",shell=True)




